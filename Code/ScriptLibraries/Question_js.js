var ChoicesList = [];
XSP.addOnLoad( function() {
	var listText = $.trim($('.choices').val());
	listText = (listText === '') ? '[]' : listText;
	ChoicesList = XSP.fromJson(listText);
	showChoiseList();
	$('#addChoice').click( function() {
		var sortkey = 0;
		$.each(ChoicesList, function(i, o) {
			sortkey = (sortkey < o.sortkey) ? o.sortkey : sortkey;
		});
		sortkey++;
		ChoicesList.push( {
			id : GetUUID(),
			sortkey : sortkey,
			name : $('#addChoiceName').val()
		});
		showChoiseList();
		$('#addChoiceName').val('');
	});
});
var showChoiseList = function() {
	var listBlock = $('#ChoiceList'), liText = '<li class="list-group-item"><span class="img bin deleteButton"></span></li>', spanText = '<span class="name"></span>';
	ChoicesList.sort( function(a, b) {
		return b.sortkey < a.sortkey;
	});
	listBlock.empty();
	$.each(ChoicesList, function(i, o) {
		var li = $(liText), span = $(spanText);
		span.text(o.name);
		li.prepend(span).appendTo(listBlock);
		if (IsEditable) {
			$('span.deleteButton', li).attr('choiceId', o.id).click(
					function() {
						deleteChoiseList($(this).attr('choiceId'));
					});
		} else {
			$('span.deleteButton', li).remove();
		}
	});

	$('.choices').val(XSP.toJson(ChoicesList));
}
var deleteChoiseList = function(id) {
	var newList = [];
	$.each(ChoicesList, function(i, o) {
		if (o.id !== id) {
			newList.push(o);
		}
	});
	ChoicesList = newList;
	showChoiseList();
}
