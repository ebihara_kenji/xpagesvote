var JSON = {
 // Makes a by-value copy of the object
 copy: function( object ){
  try {
   // Faster way to copy arrays
   if( object && typeof object.concat === 'function' ){ return object.concat(); }
   
   return this.parse( this.stringify( object ) );
  } catch( e ){ /*Debug.logException( e );*/ }
 },

 // Converts object to JSON string
 stringify: function( object ){
  try {
   return toJson( object );
  } catch( e ){ /*Debug.exception( e );*/ }
 },

 // Parses JSON to JS object
 parse: function( JSON ){
  try {
   return fromJson( '{"values":' + JSON + '}' ).values;   
  } catch( e ){ /*Debug.exception( e );*/ }  
 }
}