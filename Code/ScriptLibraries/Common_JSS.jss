var Common = {
	showDate: function(date){
		var today = @Today(),
			yesterday = @Yesterday(),
			lastYear = new Date(
				today.getFullYear() - 1,today.getMonth(), today.getDay()),
			timeFormat = new java.text.SimpleDateFormat('MM/dd HH:mm'),
			shotDateFormat = new java.text.SimpleDateFormat('MM/dd'),
			longDateFormat = new java.text.SimpleDateFormat('yyyy/MM');
		//今日の場合
		if(today <= date){
			return timeFormat.format(date);
		}
		//昨日の場合
		if(yesterday <= date){
			return 'yesterday ' + timeFormat.format(date);
		}
		//今年の場合
		if(lastYear < date){
			return shotDateFormat.format(date);
		}
		//去年の場合
		return longDateFormat.format(date);
	},
	createVector: function(array){
		var vector = new java.util.Vector();
		Common.each(array, function(item, i){
			vector.add(item);
		});
		return vector;
	},
	each:function(array,func){
		for(var i=0,max=array.length; i < max; i++){
			var ref = func(array[i], i);
			if(ref === false){
				break;
			}
		}
	}
}