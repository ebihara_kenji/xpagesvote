import Common_JSS;
var Result = {
	docStateCheck: function(){
		//この画面のQuestion文書は参照専用
		if(QuestionDoc.isEditable()){
			var exCon = facesContext.getExternalContext();
			var response = exCon.getResponse();
			response.setStatus(403);
			facesContext.responseComplete();
		}
	},
	// 結果を取得
	// 結果は選択肢のID、名前とカウントを持つオブジェクトのリスト
	getResults: function(){
		var _results = [],
		    choices,total = 0,count,
		    resultView,
		    entries;
		docId = QuestionDoc.getItemValueString('QuestionId');
		choices = fromJson(QuestionDoc.getItemValueString('Choices'));
		resultView = database.getView('Results');
		resultView.refresh();
		Common.each(choices,function(item, i){
			entries = resultView.getAllEntriesByKey(
					Common.createVector([docId, item.id])
					,true
			);
			count = entries.getCount();
			_results.push({
				id: item.id,
				name: item.name,
				count: count
			});
			total += count;
		});
		//パーセンテージの計算
		Common.each(_results, function(item,i){
			if(total === 0){
				item.per = 0;
			}else{
				item.per = Math.floor(item.count / total * 100);
			}
		});
		return _results;
	}
}