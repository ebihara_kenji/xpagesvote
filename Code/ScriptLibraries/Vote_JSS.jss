import Common_JSS;
var Vote = {
	docStateCheck: function(){
		//この画面のQuestion文書は参照専用
		if(TargetDoc.isEditable()){
			var exCon = facesContext.getExternalContext();
			var response = exCon.getResponse();
			response.setStatus(403);
			facesContext.responseComplete();
		}
	},
	setVoteDoc:function(){
		//既存の文書があればそれを取得
		//そうでない場合は、新しい文書とする
		var voteView = database.getView('Votes');
		var voteDoc:NotesDocument;
		var existDocs:NotesDocumentCollection;
		var parentQuestionId = TargetDoc.getItemValueString('QuestionId');
		existDocs = voteView.getAllDocumentsByKey(
				Common.createVector([parentQuestionId,
				session.getEffectiveUserName()]), true);
		if(existDocs.getCount() === 0){
			voteDoc = database.createDocument();
			voteDoc.replaceItemValue('form','Vote');
			voteDoc.replaceItemValue('VoteId', java.util.UUID.randomUUID().toString());
			voteDoc.replaceItemValue('QuestionId',parentQuestionId);
		}else{
			voteDoc = existDocs.getFirstDocument();
		}
		Vote.voteDocument = voteDoc;
	},
	voteDocument: null,
	save: function(){
		var voteDoc = Vote.voteDocument;
		//入力値をセット
		var choiceId = param.get('checked');
		var ValidateError:com.ibm.xsp.component.xp.XspDiv = getComponent("ValidateError");
		var SavedInformation:com.ibm.xsp.component.xp.XspDiv = getComponent("SavedInformation");
		SavedInformation.setRendered(false);
		if(!choiceId || @Trim(choiceId) === ''){
			ValidateError.setRendered(true);
			return;
		}
		ValidateError.setRendered(false);
		voteDoc.replaceItemValue('ChoiceId', choiceId);
		voteDoc.save(false,false);
		//メッセージの表示
		SavedInformation.setRendered(true);
	}
}